var url="/admin/**"//拦截的url
var nourl="/admin/login,/admin/users/login,/admin/users/logout"//忽略拦截的url
function main(ac){
	let u=session.getAttribute("admin");
	
	if(u==null){
		return ">>:admin/login";
	}else{
		if(isEmpty(u.auths)){
			u.put("auths",List.of("admin:index:"))
		}
		if(!isAuth(u.auths)){//需要权限
			return ">>:notauth"
		}
	}
}
function isAuth(auths){
	let isau=false;
	var Urls=Java.type("jse.util.Urls")
	let qx=Urls.removeSuffix(req.servletPath).substring(1).replaceAll("/",":")
	qx=qx.substring(qx.length-1,qx.length)==":"?qx:qx+":";
	if(auths!=null){
		auths.forEach(x=>{
			let x1=x.substring(x.length-1,x.length)==":"?x:x+":";
			if(qx.startsWith(x1)){
				isau=true;
			}
		})
	}
	if(!isau){
		print("该次访问需要权限:"+qx)
	}
	return isau;
}