function exportxls(tbl){
	let ExcelUtil=Java.type("cn.hutool.poi.excel.ExcelUtil")
	try {
		let filepath="d:/tomcat/webapps/upload/temp/统计数据导出.xls"
		let writer = ExcelUtil.getWriter(filepath);
		let cnd=Cnd.where("1","=",1)
		if(!isEmpty(sattr("admin").codes)){
			let codes=Arrays.asList((sattr("admin").codes+"").split(","))
			cnd.and("站点编号","in",codes)
		}
		let list=dao.query("app_all",cnd);
		
		//一次性写出内容
		writer.write(list);
		//关闭writer，释放内存
		writer.close();
		return new File(filepath);
	} catch (e) {
		print(e)
	}
}
