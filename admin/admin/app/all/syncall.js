function syncall(tbl){
	dao.clear("app_all",null);
	let list=dao.query("app_site");
	list.forEach(function(s){
		let tab=new Record();
		tab.put(".table","app_all");
		tab.put("站点编号",s.code);
		tab.put("报名总数",dao.count("app_enroll",Cnd.where("sid","=",s.code)));
		tab.put("缴费人数",dao.count("enroll",Cnd.where("sid","=",s.code).and("status","=",1)));
		tab.put("未缴费人数",dao.count("enroll",Cnd.where("sid","=",s.code).and("status","=",0)));
		tab.put("本科人数",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","本科")));
		tab.put("专科人数",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科")));
		tab.put("1法律事务",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","法律事务")));
		tab.put("2行政管理",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","行政管理")));
		tab.put("3计算机信息管理",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","计算机信息管理")));
		tab.put("4护理",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","护理")));
		tab.put("5人力资源管理",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","人力资源管理")));
		tab.put("6汽车运用与维护技术",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","汽车运用与维护技术")));
		tab.put("7会计",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","会计")));
		tab.put("8物流管理",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","专科").and("zy","=","物流管理")));
		tab.put("9法学",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","本科").and("zy","=","法学")));
		tab.put("10行政管理",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","本科").and("zy","=","行政管理")));
		tab.put("11计算机科学与技术",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","本科").and("zy","=","计算机科学与技术")));
		tab.put("12护理学",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","本科").and("zy","=","护理学")));
		tab.put("13会计学",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","本科").and("zy","=","会计学")));
		tab.put("14物流管理",dao.count("app_enroll",Cnd.where("sid","=",s.code).and("degree","=","本科").and("zy","=","物流管理")));
		dao.insert(tab)
	})
	return ">>:/admin/app/all/"
}