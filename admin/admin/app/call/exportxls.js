function exportxls(tbl){
	let ExcelUtil=Java.type("cn.hutool.poi.excel.ExcelUtil")
	try {
		let filepath="d:/tomcat/webapps/upload/temp/通话记录"+sattr("admin").phone+".xls"
		let writer = ExcelUtil.getWriter(filepath);
		let cnd=Cnd.where("1","=",1)
		if(!isEmpty(sattr("admin").phone)){
			//cnd.and("secret_no","=",sattr("admin").phone)
		}
		let list=dao.select(guokaidaochu+cnd);
		var fx={"0":"平台释放","1":"主叫释放","2":"被叫释放"}
		var yy={"1":"未分配的号码",
			    "2":"无路由到指定的转接网",
			        "3":"无路由到目的地",
			        "4":"发送专用信息音",
			        "16":"正常的呼叫拆线",
			        "17":"用户忙",
			        "18":"用户未响应",
			        "19":"用户未应答",
			        "20":"用户缺席",
			        "21":"呼叫拒收",
			        "22":"号码改变",
			        "27":"目的地不可达",
			        "28":"无效的号码格式（地址不全）",
			        "29":"性能拒绝",
			        "31":"正常—未指定",
			        "34": "无电路/通路可用",
			        "42": "交换设备拥塞",
			        "50":"所请求的性能未预定",
			        "53":"CUG中限制去呼叫",
			        "55": "CUG中限制来呼叫",
			        "57":"承载能力无权",
			        "58":"承载能力目前不可用",
			       "65":"承载能力未实现",
			        "69":"所请求的性能未实现",
			        "87":"被叫用户不是CUG的成员",
			        "88":"不兼容的目的地",
			        "90":"不存在的CUG",
			        "91":"无效的转接网选择",
			        "95":"无效的消息，未指定",
			        "97":"消息类型不存在或未实现",
			        "99":"参数不存在或未实现",
			        "102":"定时器终了时恢复",
			        "103":"参数不存在或未实现—传递",
			        "110":"消息带有未被识别的参数—舍弃",
			        "111":"协议错误,未指定",
			        "127":"互通,未指定"}
		list.forEach(function(x){
			
			x.put("释放方向",fx[x.get("释放方向")])
			x.put("释放原因",yy[x.get("释放原因")])
		})
		//一次性写出内容
		writer.write(list);
		//关闭writer，释放内存
		writer.close();
		return new File(filepath);
	} catch (e) {
		print(e)
	}
}
var guokaidaochu="SELECT\n" +
"`phone_no` AS '用户号码'," +
"`secret_no` AS '隐私号码'," +
"`peer_no` AS '学员号码'," +
"`release_dir` AS '释放方向'," +
"`release_cause` AS '释放原因'," +
"`start_time` AS '通话开始时间'," +
"`release_time` AS '通话结束时间'" +
"FROM\n" +
"app_call"