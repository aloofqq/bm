function index(tbl){
	let admin=sattr("admin")
	let sites=[]
	if(admin.role_id==1){//超级管理
		let cnd1=Cnd.where("1","=",1);
		sites=dao.query("app_site",cnd1)
	}else{
		let cnd1=Cnd.where("code","in",Arrays.asList((admin.codes+0).split(",")));
		sites=dao.query("app_site",cnd1)
	}
	attr("sites",sites)
	let cnd=Cnd.where("1","=",1)
	if(!isEmpty(tbl.tid)){
		cnd.and("tid","=",tbl.tid)
	}
	if(tbl.username||false){
		cnd.and("username","=",tbl.username)
	}
	if(tbl.start_time||false){
		cnd.and("add_time",">=",tbl.start_time)
	}
	if(tbl.end_time||false){
		cnd.and("add_time","<=",tbl.end_time)
	}
	return dao.pager("app_chat",cnd.desc("i"),tbl.page||1,20)
}