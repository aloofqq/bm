function dsh(p){
	let page=p.page||1;
	let cnd=Cnd.where("cwsh_time","=",null)
	.and("status","=",1);
	if(!isEmpty(p.orderno)){
		cnd.and("orderno","=",p.orderno)
	}
	if(!isEmpty(p.paytype)){
		cnd.and("paytype","=",p.paytype)
	}
	
	if(p.start_time||false){
		cnd.and("add_time",">=",p.start_time)
	}
	if(p.end_time||false){
		cnd.and("add_time","<=",p.end_time)
	}
	let pager=dao.pager("app_order",cnd.desc("id"),page,10)
	pager.get("list").forEach(function(o){
		if(o.status==1&&o.paytype=="wxpay"){
			let notify=dao.fetch("wxpay_notify",Cnd.where("out_trade_no","like",o.orderno+"%"))
			o.put("no",o.orderno)
			o.put("orderno",notify.out_trade_no)
		}
	})
	return pager;
}