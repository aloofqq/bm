function del(tbl){
	let enroll=dao.fetch("app_enroll",Cnd.where("id","=",tbl.id))
	let order=dao.fetch("app_order",Cnd.where("enroll_id","=",enroll.id))
	if(order.status!=1){
		try {
			if(order!=null){
				order.put(".table","app_order_del")
				dao.insert(order)
			}
			if(enroll!=null){
			enroll.put(".table","app_enroll_del")
			dao.insert(enroll)
			}
			$save({".table":"sys_log","type":"sh","tag":"删除未缴费学员",
				"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"删除未缴费学员失败",
				"username":sattr("admin").name,"param":tbl})
		} catch (e) {
			$save({".table":"sys_log","type":"sh","tag":"删除未缴费学员",
				"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"删除未缴费学员失败",
				"username":sattr("admin").name,"param":tbl})
		}
		try {
			if(order!=null){
			$del("app_order",order.id)
			}
			if(enroll!=null){
			$del("app_enroll",enroll.id)
			}
			$save({".table":"sys_log","type":"sh","tag":"备份删除未缴费学员",
				"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"备份删除未缴费学员",
				"username":sattr("admin").name,"param":tbl})
		} catch (e) {
			$save({".table":"sys_log","type":"sh","tag":"备份删除未缴费学员失败",
				"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"备份删除未缴费学员失败",
				"username":sattr("admin").name,"param":tbl})
		}
	}
	return ">>:/admin/app/enroll/"
}