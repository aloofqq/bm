function save(tbl){
	tbl.put(".table","app_enroll")
	let card=tbl.card.toUpperCase();
	tbl.put("card",card)
	if(!isEmpty(tbl.yxk)){
		let yxks=tbl.yxk.split(",")
		tbl.put("yxk",yxks[0])
		tbl.put("yxkml",yxks[1])
	}
	tbl.put("opby",sattr("admin").id)
	$update(tbl)
	$save({".table":"sys_log","type":"sh","tag":"管理员修改报名信息",
		"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"管理员修改报名信息",
		"username":sattr("admin").name,"opby":sattr("admin").id,"param":tbl})
	return ">>:/admin/app/enroll/"
}