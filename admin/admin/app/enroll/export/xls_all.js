function xls_all(tbl){
	let ExcelUtil=Java.type("cn.hutool.poi.excel.ExcelUtil")
	try {
		let filepath="d:/tomcat/webapps/upload/temp/导出全部格式.xls"
		let writer = ExcelUtil.getWriter(filepath);
		let cnd=Cnd.where("1","=",1)
		
		if(!isEmpty(tbl.status)){
			cnd.and("status","=",tbl.status)
		}
		if(!isEmpty(tbl.review)){
			if(tbl.review=="未审核"){
				cnd.and("review","=","未审核")
			}else if(tbl.review=="站点管理员审核"){
				cnd.and("site_time","NOT IS",null)
			}else if(tbl.review=="财务审核通过"){
				cnd.and("cwsh_time","NOT IS",null)
			}else if(tbl.review=="初审通过"){
				cnd.and("zrjs_time","NOT IS",null)
				cnd.and("cwsh_time","NOT IS",null)
			}else if(tbl.review=="终审通过"){
				cnd.and("zstg_time","NOT IS",null)
			}else if(tbl.review=="已入学"){
				cnd.and("rxsj_time","NOT IS",null)
			}
			
		}
		if(!isEmpty(tbl.refund)){
			cnd.and("refund","=",tbl.refund)
		}
		if(!isEmpty(tbl.sex)){
			cnd.and("sex","=",tbl.sex)
		}
		if(!isEmpty(tbl.degree)){
			cnd.and("degree","=",tbl.degree)
		}
		if(!isEmpty(tbl.code)){
			cnd.and("sid","=",tbl.code)
		}else{
			if(!isEmpty(sattr("admin").codes)){
			let codes=Arrays.asList((sattr("admin").codes+"").split(","))
			cnd.and("sid","in",codes)
			}
		}
		if(!isEmpty(tbl.start_date)&&!isEmpty(tbl.end_date)){
			cnd.and("add_time","<=",tbl.end_date)
			cnd.and("add_time",">=",tbl.start_date)
		}
		let list=dao.select(guokaidaochu+cnd);
		list.forEach(a=> {
			try {
				a.put("姓名",a.get("姓名").trim().replace(" ",""))
					if(a.付款类型=="wxpay"){
						let y=dao.fetch("wxpay_notify",Cnd.where("out_trade_no","like",a.订单编号+"-%"))
						if(y!=null)
						a.put("订单编号",y.out_trade_no)
					}
				let region=a.region.split(" ");
			a.put("籍贯(省)",region[0])
			a.put("籍贯(地市县)",region[2])
			a.put("户口所在地",region[0])
			a.remove("region")
			let refund=a.get("退款状态")
				if(refund=="0"){
					a.put("退款状态","")
				}else if(refund=="1"){
					a.put("退款状态","等待学员填写")
				}else if(refund=="2"){
					a.put("退款状态","等待责任教师审核")
				}else if(refund=="3"){
					a.put("退款状态","等待组长审核")
				}else if(refund=="4"){
					a.put("退款状态","等待财务审核")
				}
			} catch (error) {
				
			}
		});
		//一次性写出内容
		writer.write(list);
		//关闭writer，释放内存
		writer.close();
		return new File(filepath);
	} catch (e) {
		print(e)
	}
}
var guokaidaochu="SELECT\n" +
"id as '编号',\n" +
"sid as '站点编号',\n" +
"`name` as '姓名',\n" +
"sex as '性别',\n" +
"zzmm as '政治面貌',\n" +
"card as '证件号码',\n" +
"birthday as '出生日期',\n" +
"mz as '民族',\n" +
"hyzk as '婚姻状况',\n" +
"hkxz as '户口性质',\n" +
"province as '籍贯(省)',\n" +
"county as '籍贯(地市县)',\n" +
"province as '户口所在地',\n" +
"region,\n" +
"phone as '手机号码',\n" +
"city as '通讯地址',\n" +
"addr as '详细地址',\n" +
"zipcode as '邮编',\n" +
"degree as '专业层次',\n" +
"zy as '专业名称',\n" +
"isdd as '是否电大毕业',\n" +
"xlcc as '原学历层次',\n" +
"byyx as '原毕业院校',\n" +
"bysj as '毕业时间',\n" +
"yxk as '原学科',\n" +
"yxkml as '原学科门类',\n" +
"xxlx as '原学历学习类型',\n" +
"sxzy as '原学历所学专业',\n" +
"zsbh as '原学历证书编号',\n" +
"zmcl as '原学历证明材料',\n" +
"clbh as '证明材料编号',\n" +
"xlxm as '原学历姓名',\n" +
"zjlx as '原学历证件类型',\n" +
"zjhm as '原学历证件号码',\n" +
"img AS '照片地址'," +
"cardimg AS '身份证照片链接'," +
"bzkimg AS '保障卡照片链接'," +
"byzimg AS '毕业证照片链接'," +
"`rzbgimg` AS '认证报告照片链接'," +
"`zzbyimg` AS '中专毕业证书链接'," +
"`hszyimg` AS '护士执业资格证书链接'," +
"`hszmimg` AS '在职在岗证明材料链接'," +
"`babimg` AS '教育部学历证书电子注册备案表'," +
"`price` AS '学费'," +
"`remark` AS '备注'," +
"`jflx` AS '缴费类型'," +
"`status` AS '订单状态'," +
"`review` AS '审核状态'," +
"`pay_time` AS '付款时间'," +
"`cwsh_time` AS '财务审核时间'," +
"`zstg_time` AS '终审通过时间'," +
"`rxsj_time` AS '入学时间'," +
"`paytype` AS '付款类型'," +
"`sno` AS '学号'," +
"`orderno` AS '订单编号'," +
"`zrjs_id` AS '责任教师id'," +
"`zrjs_name` AS '责任教师名称'," +
"`zrjs_time` AS '责任教师审核时间'," +
"`site_time` AS '站点审核时间'," +
"`refund` AS '退款状态',"+
"`add_time` AS '报名时间'"+
"FROM\n" +
"enroll"