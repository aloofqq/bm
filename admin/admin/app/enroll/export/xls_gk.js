function xls_gk(tbl){
	let ExcelUtil=Java.type("cn.hutool.poi.excel.ExcelUtil")
	try {
		let filepath="d:/tomcat/webapps/upload/temp/导出国开格式.xls"
		let writer = ExcelUtil.getWriter(filepath);
		let cnd=Cnd.where("1","=",1)
		if(!isEmpty(tbl.status)){
			cnd.and("status","=",tbl.status)
		}
		if(!isEmpty(tbl.status)){
			cnd.and("status","=",tbl.status)
		}
		if(!isEmpty(tbl.review)){
			if(tbl.review=="未审核"){
				cnd.and("review","=","未审核")
			}else if(tbl.review=="站点管理员审核"){
				cnd.and("site_time","NOT IS",null)
			}else if(tbl.review=="财务审核通过"){
				cnd.and("cwsh_time","NOT IS",null)
			}else if(tbl.review=="初审通过"){
				cnd.and("zrjs_time","NOT IS",null)
				cnd.and("cwsh_time","NOT IS",null)
			}else if(tbl.review=="终审通过"){
				cnd.and("zstg_time","NOT IS",null)
			}else if(tbl.review=="已入学"){
				cnd.and("rxsj_time","NOT IS",null)
			}
		}
		if(!isEmpty(tbl.refund)){
			cnd.and("refund","=",tbl.refund)
		}
		if(!isEmpty(tbl.sex)){
			cnd.and("sex","=",tbl.sex)
		}
		if(!isEmpty(tbl.degree)){
			cnd.and("degree","=",tbl.degree)
		}
		if(!isEmpty(tbl.code)){
			cnd.and("sid","=",tbl.code)
		}else{
			if(!isEmpty(sattr("admin").codes)){
			let codes=Arrays.asList((sattr("admin").codes+"").split(","))
			cnd.and("sid","in",codes)
			}
		}
		if(!isEmpty(tbl.start_date)&&!isEmpty(tbl.end_date)){
			cnd.and("add_time","<=",tbl.end_date)
			cnd.and("add_time",">=",tbl.start_date)
		}
		let list=dao.select(guokaidaochu+cnd);
		list.forEach(a=> {
			a.put("姓名",a.get("姓名").trim().replace(" ",""))
			try {
				let region=a.region.split(" ");
			a.put("籍贯(省)",region[0])
			a.put("籍贯(地市县)",region[2])
			a.put("户口所在地",region[0])
			a.remove("region")
			} catch (error) {
				
			}
		});
		//一次性写出内容
		writer.write(list);
		//关闭writer，释放内存
		writer.close();
		return new File(filepath);
	} catch (e) {
		print(e)
	}
}
var guokaidaochu="SELECT\n" +
"id as '编号',\n" +
"`name` as '姓名',\n" +
"sex as '性别',\n" +
"zzmm as '政治面貌',\n" +
"'身份证' as '证件类型',\n" +
"card as '证件号码',\n" +
"birthday as '出生日期',\n" +
"mz as '民族',\n" +
"'国家机关党群组织企事业单位负责人' as '在职状况',\n" +
"'其他' as '分布情况',\n" +
"hyzk as '婚姻状况',\n" +
"'其他' as '学费来源',\n" +
"hkxz as '户口性质',\n" +
"province as '籍贯(省)',\n" +
"county as '籍贯(地市县)',\n" +
"province as '户口所在地',\n" +
"region,\n" +
"phone as '手机号码',\n" +
"'' as '固定电话',\n" +
"'' as '邮箱',\n" +
"CONCAT(city,addr) as '通讯地址',\n" +
"zipcode as '邮编',\n" +
"degree as '专业层次',\n" +
"zy as '专业名称',\n" +
"isdd as '是否电大毕业',\n" +
"xlcc as '原学历层次',\n" +
"byyx as '原毕业院校',\n" +
"bysj as '毕业时间',\n" +
"yxk as '原学科',\n" +
"yxkml as '原学科门类',\n" +
"xxlx as '原学历学习类型',\n" +
"sxzy as '原学历所学专业',\n" +
"zsbh as '原学历证书编号',\n" +
"zmcl as '原学历证明材料',\n" +
"clbh as '证明材料编号',\n" +
"xlxm as '原学历姓名',\n" +
"zjlx as '原学历证件类型',\n" +
"zjhm as '原学历证件号码',\n" +
"'' as '班级代码',\n" +
"'' as '班级名称',\n" +
"'' as '是否扶贫项目'\n" +
"FROM\n" +
"enroll"