load(Jse.jsPath+"/admin/isphone.js")
function index(p){

	let admin=sattr("admin");
	if(admin==null){
		return ">>:/admin/login"
	}
	let project_id=p.project_id||1;
	let cnd=Cnd.where("project_id","=",project_id)
	attr("project_id",project_id)
	let projects=dao.query("app_project")
	projects.forEach(function(x){
		x.put("count",dao.count("app_enroll",Cnd.where("project_id","=",x.id)))
		x.put("bkcount",dao.count("app_enroll",Cnd.where("project_id","=",x.id).and("degree","=","本科")))
		x.put("zkcount",dao.count("app_enroll",Cnd.where("project_id","=",x.id).and("degree","=","专科")))
	})
	attr("projects",projects)
	
	let scnd=Cnd.where("1","=",1)
	if(!isEmpty(admin.codes)){
		let codes=Arrays.asList(admin.codes.split(","))
		scnd.and("code","in",codes)
		cnd.and("sid","in",codes)
	}
	attr("sites",dao.query("app_site",scnd))
	
	attr("zys",dao.query("app_zy"))
	
	if(p.code||false){
		cnd.and("sid","=",p.code)
	}
	if(p.degree||false){
		cnd.and("degree","=",p.degree)
	}
	if(p.zy||false){
		cnd.and("zy","=",p.zy)
	}
	if(p.status||false){
		cnd.and("status","=",p.status)
	}
	if(p.sh||false){
		if(p.sh=="ws"){
			cnd.and("review","=","未审核")
		}else if(p.sh=="js"){
			cnd.and("zrjs_time","NOT IS",null)
		}else if(p.sh=="cw"){
			cnd.and("cwsh_time","NOT IS",null)
		}else if(p.sh=="cs"){
			cnd.and("zrjs_time","NOT IS",null)
			.and("cwsh_time","NOT IS",null)
		}else if(p.sh=="zs"){
			cnd.and("zstg_time","NOT IS",null)
		}else if(p.sh=="rx"){
			cnd.and("rxsj_time","NOT IS",null)
		}
	}
	if(p.hyzk||false){
		cnd.and("hyzk","=",p.hyzk)
	}
	if(p.sex||false){
		cnd.and("sex","=",p.sex)
	}
	if(p.paytype||false){
		cnd.and("paytype","=",p.paytype)
	}
	if(p.orderno||false){
		cnd.and("orderno","=",p.orderno)
	}
	if(p.name||false){
		cnd.and("name","like","%"+p.name+"%")
	}
	if(p.phone||false){
		cnd.and("phone","like","%"+p.phone+"%")
	}
	if(p.card||false){
		cnd.and("card","like","%"+p.card+"%")
	}
	if(p.start_time||false){
		cnd.and("add_time",">=",p.start_time)
	}
	if(p.end_time||false){
		cnd.and("add_time","<=",p.end_time)
	}
	if(isphone(req)){
		attr("page","admin/m/enroll/index")
	}
	let page=p.page||1;
	var pager=dao.pager("enroll",cnd.desc("id"),page,10);
	if(pager.list.size()<10&&p.page!="1"&&pager.pager.recordCount<10){
		pager=dao.pager("enroll",cnd.desc("id"),1,10);
	}
	pager.list.forEach(function(x){
		x.put("is_up",dao.count("app_enroll_up",Cnd.where("id","=",x.id)))
	})
	return pager;
}