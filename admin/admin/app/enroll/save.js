function save(tbl){
	if(!isEmpty(tbl.yxk)){
		let yxks=tbl.yxk.split(",")
		tbl.put("yxk",yxks[0])
		tbl.put("yxkml",yxks[1])
	}
	let region=tbl.region;
	if(region!=null){
		tbl.put("province",region.province)
		tbl.put("area",region.area)
		tbl.put("county",region.county)
	}
	$update(tbl)
	return ">>:/admin/app/enroll/view?id="+tbl.id
}