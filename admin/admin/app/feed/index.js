function index(tbl) {
	let cnd = Cnd.where("pid", "=", 0)
	let codes = Arrays.asList((sattr("admin").codes + "0").split(","))
	let scnd = Cnd.where("1", "=", 1)
	if (!isEmpty(sattr("admin").codes)) {
		scnd.and("code", "in", codes)
	}
	attr("sites", dao.query("app_site", scnd))

	
	if (!isEmpty(tbl.code)) {
		cnd.and("code", "=", tbl.code)
	}else{
		if (!isEmpty(sattr("admin").codes)) {
			cnd.and("code", "in", codes)
		}
	}
	if (!isEmpty(tbl.title)) {
		cnd.and("title", "=", tbl.title)
	}
	let page=tbl.page || 1;
	let pager = dao.pager("app_feed", cnd.desc("id"), page, 20)
	pager.list.forEach(function(x){
		x.put("list",dao.query("app_feed",Cnd.where("pid","=",x.id)))
	})
	return pager;
}