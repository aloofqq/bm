function index(p){
	let page=p.page||1;
	let cnd=Cnd.where("1","=",1)
	if(p.status||false){
		cnd.and("status","=",p.status)
	}
	if(p.paytype||false){
		cnd.and("paytype","=",p.paytype)
	}
	if(p.orderno||false){
		cnd.and("orderno","like",p.orderno+"%")
	}
	if(p.start_time||false){
		cnd.and("add_time",">=",p.start_time)
	}
	if(p.end_time||false){
		cnd.and("add_time","<=",p.end_time)
	}
	print(cnd)
	let pager=dao.pager("app_order",cnd.desc("id"),page,10)
	pager.get("list").forEach(function(o){
		if(o.status==1&&o.paytype=="wxpay"){
			let notify=dao.fetch("wxpay_notify",Cnd.where("out_trade_no","like",o.orderno+"%"))
			o.put("no",o.orderno)
			o.put("orderno",notify.out_trade_no)
		}
	})
	//attr("sites",dao.query("app_site"))
	return pager;
}