function index(tbl){
	let page=tbl.page||1;
	let pager=dao.pager("app_order",Cnd.where("refund","=",4).desc("id"),page,20)
	pager.list.forEach(function(x){
		x.put("refund",dao.fetch("app_refund",Cnd.where("orderno","=",x.orderno)))
		if(x.paytype=="wxpay"){
			let y=dao.fetch("wxpay_notify",Cnd.where("out_trade_no","like",x.orderno+"-%"))
			if(y!=null)
			x.put("orderno",y.out_trade_no)
		}
	})
	return pager;
}