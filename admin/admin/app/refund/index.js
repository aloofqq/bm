load(Jse.jsPath+"/admin/isphone.js")
function index(p){
	let project_id=p.project_id||1;
	let cnd=Cnd.where("project_id","=",1)
	
	let codes=Arrays.asList((sattr("admin").codes+"0").split(","))
	let scnd=Cnd.where("1","=",1)
	if(!isEmpty(sattr("admin").codes)){
		scnd.and("code","in",codes)
		cnd.and("sid","in",codes)
	}
	attr("sites",dao.query("app_site",scnd))
	
	if(p.code||false){
		cnd.and("sid","=",p.code)
	}
	if(p.degree||false){
		cnd.and("degree","=",p.degree)
	}
	if(p.zy||false){
		cnd.and("zy","=",p.zy)
	}
	if(p.status||false){
		cnd.and("status","=",p.status)
	}
	if(p.sh||false){
		if(p.sh=="ws"){
			cnd.and("review","=","未审核")
		}else if(p.sh=="js"){
			cnd.and("zrjs_time","NOT IS",null)
		}else if(p.sh=="cw"){
			cnd.and("cwsh_time","NOT IS",null)
		}else if(p.sh=="cs"){
			cnd.and("zrjs_time","NOT IS",null)
		}else if(p.sh=="zs"){
			cnd.and("zstg_time","NOT IS",null)
		}else if(p.sh=="rx"){
			cnd.and("rxsj_time","NOT IS",null)
		}
	}
	if(p.hyzk||false){
		cnd.and("hyzk","=",p.hyzk)
	}
	if(p.sex||false){
		cnd.and("sex","=",p.sex)
	}
	if(p.paytype||false){
		cnd.and("paytype","=",p.paytype)
	}
	if(p.orderno||false){
		cnd.and("orderno","=",p.orderno)
	}
	if(p.name||false){
		cnd.and("name","like","%"+p.name+"%")
	}
	if(p.phone||false){
		cnd.and("phone","like","%"+p.phone+"%")
	}
	if(p.card||false){
		cnd.and("card","like","%"+p.card+"%")
	}
	if(p.start_time||false){
		cnd.and("add_time",">=",p.start_time)
	}
	if(p.end_time||false){
		cnd.and("add_time","<=",p.end_time)
	}
	if(isphone(req)){
		attr("page","admin/m/enroll/index")
	}
	cnd.and("status","=",1).and("refund",">",2)
	var pager=dao.pager("enroll",cnd.desc("id"),p.page||1,10);
	pager.get("list").forEach(function(o){
		if(o.status==1&&o.paytype=="wxpay"){
			let notify=dao.fetch("wxpay_notify",Cnd.where("out_trade_no","like",o.orderno+"%"))
			o.put("no",o.orderno)
			o.put("orderno",notify.out_trade_no)
		}
	})
	return pager;
}