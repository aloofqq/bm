function refund(tbl){
	let rid=sattr("admin").role_id;
	tbl.put(".table","app_order")
	if(rid==2){//教师
		dao.update("app_refund",Chain.make("zrjs_id",sattr("admin").id)
				.add("zrjs_name",sattr("admin").name)
				.add("admin_time",new java.util.Date()),Cnd.where("oid","=",tbl.id))
			$update(tbl)
			$save({".table":"sys_log","type":"sh","tag":"责任教师确认退款申请",
			"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"责任教师确认退款申请",
			"username":sattr("admin").name,"param":tbl})
	}else if(rid==5){//组长
		dao.update("app_refund",Chain.make("admin_id",sattr("admin").id)
				.add("admin_name",sattr("admin").name)
				.add("admin_time",new java.util.Date()),Cnd.where("oid","=",tbl.id))
			$update(tbl)
			$save({".table":"sys_log","type":"sh","tag":"组长确认退款申请",
			"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"组长确认退款申请",
			"username":sattr("admin").name,"param":tbl})
	}else if(rid==4){//财务
		try {
			let order=dao.fetch("app_order",Cnd.where("id","=",tbl.id))
			
			dao.update("app_refund",Chain.make("cwsh_time",new java.util.Date()).add("status",1),Cnd.where("orderno","=",order.orderno))
			let enroll=dao.fetch("app_enroll",Cnd.where("id","=",order.enroll_id))
			order.put(".table","app_order_del")
			enroll.put(".table","app_enroll_del")
			Trans.exec(function(){
				try {
					dao.insert(order)
				} catch (e1) {
				}
				try {
					dao.insert(enroll)
				} catch (e2) {
				}
				try {
					var sql=Sqls.create("DELETE FROM app_order WHERE orderno = '"+order.orderno+"'");
					dao.execute(sql);
				} catch (e3) {
				}
				try {
					var sql1=Sqls.create("DELETE FROM app_enroll WHERE id = "+enroll.id+"");
					dao.execute(sql1);
				} catch (e4) {
				}
				
			});
			let tab=new Record();
			tab.put("phone",enroll.phone)
			tab.put("tmpl","SMS_142151080")
			let Sms=Java.type("jse.sms.Sms");
			var res = Sms.send(tab);
			res.put(".table","sms_result")
			res.put("tmpl","SMS_142151080")
			dao.insert(res)
		} catch (e) {
			print(e)
		}
		$save({".table":"sys_log","type":"sh","tag":"财务确认退款申请",
			"src":"admin.app.enroll.sh.js","ip":ip(),"msg":"财务确认退款申请",
			"username":sattr("admin").name,"param":tbl})
	}
	return {"code":200}
}