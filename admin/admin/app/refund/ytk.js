 function ytk(p){
	let project_id=p.project_id||1;
	let cnd=Cnd.where("project_id","=",1)
	
	let codes=Arrays.asList((sattr("admin").codes+"0").split(","))
	let scnd=Cnd.where("1","=",1)
	if(!isEmpty(sattr("admin").codes)){
		scnd.and("code","in",codes)
		cnd.and("sid","in",codes)
	}
	attr("sites",dao.query("app_site",scnd))
	
	if(p.code||false){
		cnd.and("sid","=",p.code)
	}
	if(p.degree||false){
		cnd.and("degree","=",p.degree)
	}
	if(p.zy||false){
		cnd.and("zy","=",p.zy)
	}
	if(p.status||false){
		cnd.and("status","=",p.status)
	}
	if(p.sh||false){
		if(p.sh=="ws"){
			cnd.and("review","=","未审核")
		}else if(p.sh=="js"){
			cnd.and("zrjs_time","NOT IS",null)
		}else if(p.sh=="cw"){
			cnd.and("cwsh_time","NOT IS",null)
		}else if(p.sh=="cs"){
			cnd.and("zrjs_time","NOT IS",null)
		}else if(p.sh=="zs"){
			cnd.and("zstg_time","NOT IS",null)
		}else if(p.sh=="rx"){
			cnd.and("rxsj_time","NOT IS",null)
		}
	}
	if(p.hyzk||false){
		cnd.and("hyzk","=",p.hyzk)
	}
	if(p.sex||false){
		cnd.and("sex","=",p.sex)
	}
	if(p.paytype||false){
		cnd.and("paytype","=",p.paytype)
	}
	if(p.orderno||false){
		let orderno=p.orderno;
		if(orderno.indexOf("-")>-1)
			orderno=orderno.substring(0, orderno.indexOf("-"));
		cnd.and("orderno","=",orderno)
	}
	if(p.name||false){
		cnd.and("name","like","%"+p.name+"%")
	}
	if(p.phone||false){
		cnd.and("phone","like","%"+p.phone+"%")
	}
	if(p.card||false){
		cnd.and("card","like","%"+p.card+"%")
	}
	if(p.start_time||false){
		cnd.and("add_time",">=",p.start_time)
	}
	if(p.add_time||false){
		cnd.and("add_time","like",p.add_time+" %")
	}
	cnd.and("status","=",1).and("refund","=",4)
	let page=p.page||1;
	let limit=p.limit||10;
	var pager=dao.pager("enroll_del",cnd.desc("id"),page,limit);
	pager.put("code",200)
	pager.put("count",pager.pager.recordCount)
	pager.list.forEach(function(x){
		if(x.refund==4){
			x.put("refundtxt","已退款订单")
		}
		x.put("no",x.orderno)
		if(x.paytype=="wxpay"){
			let y=dao.fetch("wxpay_notify",Cnd.where("out_trade_no","like",x.orderno+"-%"))
			if(y!=null)
			x.put("orderno",y.out_trade_no)
		}
	})
	return pager;
}