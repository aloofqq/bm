function index(tbl){
	print(tbl)
	if(!isEmpty(tbl.file)){
		try {
			return cwsh(tbl);
		} catch (e) {
			e.printStackTrace();
		}
	}
	return null;
}

function cwsh(tbl){
	var list=[];
	var type=""
	if(Fs.getSuffix(tbl.file)==".csv"){
		list=csv(tbl);
	}else{
		var ExcelReader=Java.type("cn.hutool.poi.excel.ExcelReader")
		list=new ExcelReader("D:/tomcat/webapps/"+tbl.file,0).read()
	}
	var i = 0;
	if(list.get(0).toString().startsWith("[#账号")){
		i=3;//支付宝
		type="alipay"
	}else{
		i=4;//微信
		type="wx"
	}
	let rts=new ArrayList();
	//print(list)
	for (;i < list.size(); i++) {
		let x=list.get(i);
		//print(x)
		var orderno="";
		if(type=="wx"){//微信
			let rt=new Record();
			rt.put("type","微信")
			rt.put("status","未找到订单")
			rt.put("no",x[1])
			rt.put("orderno",x[2])
			
			if(x[2].toString().indexOf("-")>0) {
				let index1=0;
				if(x[2].toString().indexOf("`")>-1){
					index1=1
				}
				orderno=x[2].toString().substring(index1,x[2].indexOf("-"));
			}else {
				orderno=x[2].toString();
			}
			if((x.length==6&&x[4].toString()=="买家已支付")||
					(x.length==12&&x[9].toString()=="买家已支付")){
				let order=dao.fetch("app_order",Cnd.where("orderno","like",orderno+"%"))
				let review="财务审核通过"
				let cstg_time=null;
				if(order==null){
					rt.put("status","订单不存在")
				}else if(!isEmpty(order.zrjs_time)){
					review="初审通过"
					cstg_time=new java.util.Date();
					rt.put("status",review)
					dao.update("app_order",Chain.make("review",review).add("cwsh_time",new java.util.Date())
							.add("cstg_time",cstg_time),Cnd.where("id","=",order.id));
				}else{
					rt.put("status",review)
					dao.update("app_order",Chain.make("review",review).add("cwsh_time",new java.util.Date())
							.add("cstg_time",cstg_time),Cnd.where("id","=",order.id));
				}
			}
			rts.add(rt)
		}else if(!x[0].startsWith("#导出时间")){//支付宝
			let rt=new Record();
			rt.put("type","支付宝")
			rt.put("status","未找到订单")
			orderno=x[3];
			orderno=orderno.replaceAll("，",",")
			rt.put("no",x[1])
			rt.put("orderno",orderno)
			if(x[10]=="成功"){
				let order=dao.fetch("app_order",Cnd.where("orderno","=",orderno))
				let review="财务审核通过"
				let cstg_time=null;
				if(order==null){
					rt.put("status","订单不存在")
				}else if(!isEmpty(order.zrjs_time)){
					review="初审通过"
					cstg_time=new java.util.Date();
					rt.put("status",review)
				}else{
					rt.put("status",review)
				}
				dao.update("app_order",Chain.make("review",review).add("cwsh_time",new java.util.Date())
						.add("cstg_time",cstg_time),Cnd.where("orderno", "=", orderno));
			}
			rts.add(rt)
		}
		
	};
	return rts;
}
function csv(tbl){
	var CsvReader=Java.type("cn.hutool.core.text.csv.CsvReader")
	let f=new File("D:/tomcat/webapps/"+tbl.file)
	let FileEncoding=Java.type("jse.util.FileEncoding")
	let charset=FileEncoding.detect(f)
	print(charset)
	let d=new CsvReader().read(f,Charset.forName(charset));
	var list=new ArrayList();;
	for (let i = 0; i < d.getRowCount(); i++) {
		let row=d.getRow(i);
		let list1=new ArrayList();
		row.forEach(x=>{
			list1.add(x);
		});
		list.add(list1);
	}
	return list;
}