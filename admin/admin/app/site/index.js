function index(tbl){
	let cnd=Cnd.where("1","=",1)
	if(!isEmpty(tbl.key)&&!isEmpty(tbl.val)){
		cnd.and(tbl.key,"like","%"+tbl.val+"%")
	}
	attr("projects",dao.query("app_project"))
	let codes = Arrays.asList((sattr("admin").codes + "0").split(","))
	if (!isEmpty(sattr("admin").codes)) {
		cnd.and("code", "in", codes)
		
	}
	return dao.pager("app_site",cnd.desc("id"),tbl.page||1,20)
}