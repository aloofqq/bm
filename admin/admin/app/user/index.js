function index(p){
	let cnd=Cnd.where("1","=",1)
	let codes=Arrays.asList((sattr("admin").codes+"0").split(","))
	let scnd=Cnd.where("1","=",1)
	if(!isEmpty(sattr("admin").codes)){
		scnd.and("code","in",codes)
		cnd.and("code","in",codes)
	}
	attr("sites",dao.query("app_site",scnd))
	if(p.code||false){
		cnd.and("code","=",p.code)
	}
	if(p.name||false){
		cnd.and("name","=",p.name)
	}
	if(p.card||false){
		cnd.and("card","like",p.card+"%")
	}
	if(p.phone||false){
		cnd.and("phone","like",p.phone+"%")
	}
	if(p.add_time||false){
		cnd.and("add_time",">=",p.add_time)
	}
	let page=p.page||1;
	let limit=p.limit||10;
	var pager=dao.pager("users",cnd.desc("id"),page,limit);
	pager.put("code",200)
	pager.put("count",pager.pager.recordCount)
	return pager;
}