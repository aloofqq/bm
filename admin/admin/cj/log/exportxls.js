function exportxls(tbl){
	let ExcelUtil=Java.type("cn.hutool.poi.excel.ExcelUtil")
	try {
		let filepath="d:/tomcat/webapps/upload/temp/查询日志导出.xls"
		let writer = ExcelUtil.getWriter(filepath);
		let cnd=Cnd.where("1","=",1)
		if(!isEmpty(tbl.start_date)&&!isEmpty(tbl.end_date)){
			cnd.and("add_time","<=",tbl.end_date)
			cnd.and("add_time",">=",tbl.start_date)
		}
		let list=dao.select("select * from cj_log "+cnd);
		//一次性写出内容
		writer.write(list);
		//关闭writer，释放内存
		writer.close();
		return new File(filepath);
	} catch (e) {
		print(e)
	}
}