function main(tbl){
	let admin=sattr("admin")
	if(admin==null){
		return ">>:/admin/login"
	}
	var cnd1=Cnd.where("1","=",1)
	var cnd2=Cnd.where("status","=",1)
	var cnd3=Cnd.where("status","=",0)
	var cnd4=Cnd.where("status","=",0)
	var cnd5=Cnd.where("0","=",0)
	var cnd7=Cnd.where("cwsh_time","is not",null)
	if(!isEmpty(admin.codes)){
		let codes=Arrays.asList((admin.codes+"").split(","))
		cnd1=cnd1.and("sid","in",codes);
		cnd2=cnd2.and("sid","in",codes);
		cnd3=cnd3.and("sid","in",codes);
		cnd4=cnd4.and("code","in",codes);
		cnd5=cnd5.and("sid","in",codes);
		cnd7=cnd5.and("sid","in",codes);
	}
	attr("num1",dao.count("app_enroll",cnd1))
	attr("num2",dao.count("enroll",cnd2))
	attr("num3",dao.count("enroll",cnd3))
	attr("num4",dao.count("app_refund",cnd4))
	attr("num5",dao.count("app_enroll_up",cnd5))
	attr("num6",dao.count("app_feed",Cnd.where("pid","=",0)))
	attr("num7",dao.count("enroll",cnd7))
	attr("num8",dao.count("app_call"))
	let md=getymd();
	//print(ymd(1))
	let ymd_bb=new Record();
	let ymd_bb_day=new ArrayList();
	let ymd_bb_num1=new ArrayList();
	let ymd_bb_num2=new ArrayList();
	/*for(let i=1;i<=md.day;i++){
		ymd_bb_day.add(i<10?"0"+i:i+"")
		
		let cndymd1=Cnd.where(new Static("TO_DAYS(add_time) = TO_DAYS('"+Times.format(ymd(i))+"')"))
		let cndymd2=Cnd.where(new Static("TO_DAYS(add_time) = TO_DAYS('"+Times.format(ymd(i))+"')"))
					.and("status","=",1)
		if(!isEmpty(admin.codes)){
			let codes=Arrays.asList((sattr("admin").codes+"").split(","))
			cndymd1.and("sid","in",codes);
			cndymd2.and("sid","in",codes);
		}
		let num1=dao.count("enroll",cndymd1)
		let num2=dao.count("enroll",cndymd2)
		ymd_bb_num1.add(num1)
		ymd_bb_num2.add(num2)
	}*/
	ymd_bb.put("day",ymd_bb_day)
	ymd_bb.put("num1",ymd_bb_num1)
	ymd_bb.put("num2",ymd_bb_num2)
	attr("ymd_bb",ymd_bb)
	/*let zfcnd1=Cnd.where("paytype","=","alipay")
	let zfcnd2=Cnd.where("paytype","=","wxpay")
	let zfcnd3=Cnd.where("status","=",0)
	if(!isEmpty(admin.codes)){
		let codes=Arrays.asList((sattr("admin").codes+"").split(","))
		zfcnd1.and("sid","in",codes);
		zfcnd2.and("sid","in",codes);
		zfcnd3.and("sid","in",codes);
	}
	let zf1=Record.create().set("name", "支付宝")
	.set("value", dao.count("app_order",zfcnd1))
	let zf2=Record.create().set("name", "微信")
	.set("value", dao.count("app_order",zfcnd2))
	let zf3=Record.create().set("name", "未支付")
	.set("value", dao.count("app_order",zfcnd3))
	let zf_bb=List.of(zf1,zf2,zf3);
	attr("zf_bb",zf_bb)
	
	let zhou_bb=new Record();
	let zhou_bb_day=new ArrayList();
	let zhou_bb_num=new ArrayList();
	for(let i=7;i>=1;i--){
		zhou_bb_day.add(Times.format("MM-dd",ymw(-i)))
		let zhoucnd=Cnd.where(new Static("TO_DAYS(reg_time) = TO_DAYS('"+Times.format(ymw(-i))+"')"))
		if(!isEmpty(admin.codes)){
			let codes=Arrays.asList((sattr("admin").codes+"").split(","))
			zhoucnd.and("code","in",codes);
		}
		let num1=dao.query("users",zhoucnd)
		zhou_bb_num.add(num1)
	}
	zhou_bb.put("day",zhou_bb_day)
	zhou_bb.put("num",zhou_bb_num)
	attr("zhou_bb",zhou_bb)*/
	
	let zy_bb=new Record();
	let zy_bb_zy=dao.select("select name from app_zy","lists")
	zy_bb_zy.add(0,"专科")
	zy_bb_zy.add(1,"本科")
	zy_bb.put("name",zy_bb_zy)
	let zys=dao.query("app_zy");
	let zyscnd1=Cnd.where("degree","=","专科")
	let zyscnd2=Cnd.where("degree","=","本科")
	if(!isEmpty(admin.codes)){
		let codes=Arrays.asList((admin.codes+"").split(","))
		zyscnd1.and("sid","in",codes);
		zyscnd1.and("sid","in",codes);
	}
	for (var i = 0; i < zys.size(); i++) {
		let x=zys.get(i)
		if(!isEmpty(admin.codes)){
			let codes=Arrays.asList((admin.codes+"").split(","))
			zys.get(i).put("value",dao.count("app_enroll",Cnd.where("degree","=",x.degree)
					.and("zy","=",x.name).and("sid","=",codes)))
		}else{
			zys.get(i).put("value",dao.count("app_enroll",Cnd.where("degree","=",x.degree).and("zy","=",x.name)))
		}
	}
	zys.add(0,Record.create().set("name","专科").set("value",dao.count("app_enroll",zyscnd1)))
	zys.add(1,Record.create().set("name","本科").set("value",dao.count("app_enroll",zyscnd2)))
	zy_bb.put("zys",zys)
	attr("zy_bb",zy_bb)
	let pros=["北京","天津","上海","重庆","河北","山西","辽宁","吉林","黑龙江","江苏","浙江","安徽","福建","江西","山东","河南","湖北","湖南","广东","海南","四川","贵州","云南","陕西","甘肃","青海","内蒙古","广西","西藏","宁夏","新疆","香港","澳门","台湾"];
	let citys=new ArrayList();
	for (var i = 0; i < pros.length; i++) {
		if(!isEmpty(admin.codes)){
			let codes=Arrays.asList((admin.codes+"").split(","))
			citys.add(Record.create().set("name",pros[i])
					.set("value",dao.count("app_enroll",Cnd.where("province","like",pros[i]+"%").and("sid","=",codes))))
		}else{
			citys.add(Record.create().set("name",pros[i])
					.set("value",dao.count("app_enroll",Cnd.where("province","like",pros[i]+"%"))))
		}
		
	}
	attr("citys",citys)
	print(admin)
	if(admin.role_id==1){
		attr("page","/admin/main1")
	}else if(admin.role_id==2){
		attr("page","/admin/main2")
	}else if(admin.role_id==3){
		attr("page","/admin/main3")
	}else if(admin.role_id==4){
		attr("page","/admin/main4")
	}else if(admin.role_id==5){
		attr("page","/admin/main5")
	}else{
		attr("page","/admin/main1")
	}
}

function getymd(){
	let a = Calendar.getInstance();  
    a.set(Calendar.DATE, 1);//把日期设置为当月第一天  
    a.roll(Calendar.DATE, -1);//日期回滚一天，也就是最后一天  
    let day = a.get(Calendar.DATE);  
    let month = a.get(Calendar.MONTH)+1;  
    let year = a.get(Calendar.YEAR);  
    return {"day":day,"month":month,"year":year};
}
function ymd(day){
	let a = Calendar.getInstance();  
    a.set(Calendar.DATE, day);//把日期设置为当月第一天  
    return a.getTime();
}
function ymw(day){//前后几天
	let a = Calendar.getInstance();  
    a.add(Calendar.DATE, day);//把日期设置为当月第一天  
    return a.getTime();
}