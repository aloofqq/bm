function index(tbl){
	let cnd=Cnd.where("1","=",1)
	if(!isEmpty(tbl.type)){
		cnd.and("tmpl","=",tbl.type)
	}
	if(!isEmpty(tbl.phone)){
		cnd.and("phone","like","%"+tbl.phone+"%")
	}
	if(tbl.start_time||false){
		cnd.and("add_time",">=",tbl.start_time)
	}
	if(tbl.end_time||false){
		cnd.and("add_time","<=",tbl.end_time)
	}
	var wcs=dao.pager("sms_result",cnd.desc("id"),tbl.page||1,20)
	return wcs;
}
