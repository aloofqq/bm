function update(tbl){
let rsm =  Packages.org.apache.shiro.SecurityUtils.getSecurityManager();
	//AccountAuthorizationRealm为在项目中定义的realm类
	let shiroRealm = rsm.getRealms().iterator().next();
	let subject = Packages.org.apache.shiro.SecurityUtils.getSubject();
	//用realm删除principle
	shiroRealm.getAuthorizationCache().remove(subject.getPrincipals());
	//切换身份也就是刷新了
	subject.releaseRunAs();
	return {code:200}
}