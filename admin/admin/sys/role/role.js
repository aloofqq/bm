function role(tbl){
	var role_menus=dao.select("select menu_id from sys_role_menu "+Cnd.where("role_id","=",tbl.id),"lists")
	let menus=dao.query("sys_menu",Cnd.where("1","=",1));
	menus.forEach(function(m){
		if(role_menus.contains(m.id)){
			m.put("checked",true)
		}
	})
	attr("menus",menus)
	var role_units=dao.select("select unit_id from sys_role_unit "+Cnd.where("role_id","=",tbl.id),"lists")
	let units=dao.query("sys_unit");
	units.forEach(function(m){
		if(role_units.contains(m.id)){
			m.put("checked",true)
		}
	})
	attr("units",units)
	return dao.fetch("sys_role",Cnd.where("id","=",tbl.id));
}