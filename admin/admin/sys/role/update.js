function update(tbl){
	let list=new ArrayList();
	tbl.mids.forEach(function(x){
		let tab=new Record();
		tab.put(".table","sys_role_menu")
		tab.put("role_id",tbl.id)
		tab.put("menu_id",x)
		list.add(tab);
	})
	Trans.exec(function(){
		dao.clear("sys_role_menu",Cnd.where("role_id","=",tbl.id))
		dao.fastInsert(list)
    });
	/*let rsm =  Packages.org.apache.shiro.SecurityUtils.getSecurityManager();
	//AccountAuthorizationRealm为在项目中定义的realm类
	let shiroRealm = rsm.getRealms().iterator().next();
	let subject = Packages.org.apache.shiro.SecurityUtils.getSubject();
	//用realm删除principle
	shiroRealm.getAuthorizationCache().remove(subject.getPrincipals());
	//切换身份也就是刷新了
	subject.releaseRunAs();*/
	return {code:200};
}