function enable(tbl){
	try {
		let sysTask = dao.fetch("sys_task",Cnd.where("id","=",tbl.id));
        try {
        	let isExist = quartzManager.exist(new JobKey(sysTask.id, sysTask.id));
            if (!isExist) {
                let qj = new QuartzJob();
                qj.setJobName(sysTask.id);
                qj.setJobGroup(sysTask.id);
                qj.setClassName(sysTask.jobclass);
                qj.setCron(sysTask.cron);
                qj.setComment(sysTask.note);
                qj.setDataMap(sysTask.data);
                quartzManager.add(qj);
            }
        } catch (e) {
            print(e);
        }
        print(`启用任务${sysTask.name}成功`)
        dao.update("sys_task",Chain.make("disabled", false),Cnd.where("id", "=",tbl.id));
        attr("_httpurl","/admin/sys/task/")
        return {code:200,msg:"success"};
    } catch (e) {
    	print(e)
        return {code:201,msg:"fail"};
    }
}