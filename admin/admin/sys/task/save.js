function save(tbl){
	if(isEmpty(tbl.id)){
		tbl.put("id",uuid())
		try {
			$save(tbl)
            let sysTask = tbl;
            try {
                if (sysTask.disabled==0) {
                    let qj = new QuartzJob();
                    qj.setJobName(sysTask.id);
                    qj.setJobGroup(sysTask.id);
                    qj.setClassName(sysTask.jobclass);
                    qj.setCron(sysTask.cron);
                    qj.setComment(sysTask.note);
                    qj.setDataMap(sysTask.data);
                    quartzManager.add(qj);
                }
            } catch (e) {
               print(e);
            }
            return ">>:/admin/sys/task/";
        } catch (e) {
            return {code:201,msg:e};
        }
	}else{
		return update(tbl)
	}
}
function update(tbl){
	try {
        let sysTask=tbl;
        try {
            let isExist = quartzManager.exist(new JobKey(sysTask.id, sysTask.id));
            if (isExist) {
                let qj = new QuartzJob();
                qj.setJobName(sysTask.id);
                qj.setJobGroup(sysTask.id);
                quartzManager.delete(qj);
            }
            if (sysTask.disabled==0) {
                let qj = new QuartzJob();
                qj.setJobName(sysTask.id);
                qj.setJobGroup(sysTask.id);
                qj.setClassName(sysTask.jobclass);
                qj.setCron(sysTask.cron);
                qj.setComment(sysTask.note);
                qj.setDataMap(sysTask.data);
                quartzManager.add(qj);
            }
        } catch (e) {
            print(e);
        }
        $update(sysTask);
        return ">>:/admin/sys/task/";
    } catch (e) {
        return {code:201,msg:e};
    }
}