function importxls(tbl){
	let line=0;
	let ExcelUtil=Java.type("cn.hutool.poi.excel.ExcelUtil")
	try {
		if(!isEmpty(tbl.file)){
		let filepath="d:/tomcat/webapps/"+tbl.file;
		let reader = ExcelUtil.getReader(filepath);
		let list = reader.readAll();
		let tablist=new ArrayList();
		
		list.forEach(function(x){
			line++;
			let tab=new Record();
			tab.put(".table","sys_user")
			tab.put("username",x.get("手机号"))
			tab.put("name",x.get("姓名"))
			tab.put("post",x.get("职务"))
			tab.put("phone",x.get("隐私号码"))
			tab.put("role_id",x.get("角色id"))
			tab.put("remark",x.get("描述"))
			let sys_user=dao.fetch("sys_user",Cnd.where("username","=",x.get("手机号")))
			if(sys_user==null){
			let codes=x.get("站点编号")
			if(!isEmpty(codes)){
				let c=codes.charAt(codes.length-1)
				if(c!=','){
					tab.put("codes",codes+",")
				}else{
					tab.put("codes",codes)
				}
			}
				dao.insert(tab)
			}else{
				let codes=x.get("站点编号")
				if(!isEmpty(codes)){
					if(codes.charAt(codes.length-1)==','){
						codes=codes.substring(0,codes.length-1)
					}
				}
				let site=dao.fetch("app_site",Cnd.where("code","=",codes))
				tab.put("id",sys_user.id)
				if(site==null){
					//该站点不存在
					print(sys_user.codes+"不存在")
				}else if(isEmpty(sys_user.codes)){
					
					tab.put("codes",codes+",")
				}else if(sys_user.codes.contains(codes+",")){
					
				}else{
					tab.put("codes",sys_user.codes+codes+",")
				}
				$update(tab)
			}
		})
		}
		return ">>:/admin/sys/user/?line="+line;
	} catch (e) {
		print(e)
		return ">>:/admin/sys/user/?err=ferr"
	}
	
}