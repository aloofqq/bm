function index(p){
	let cnd=Cnd.where("0","=",0);
	if(p.code||false){
		cnd.and("codes","like","%"+p.code+",%")
	}
	if(p.name||false){
		cnd.and("name","=",p.name)
	}
	if(p.username||false){
		cnd.and("username","=",p.username)
	}
	if(p.phone||false){
		cnd.and("phone","=",p.phone)
	}
	let page=p.page||1;
	attr("sites",dao.query("app_site"))
	let pager=dao.pager("sys_user",cnd.desc("id"),page,20);
	pager.list.forEach(function(x){
		x.put("role",dao.fetch("sys_role",Cnd.where("id","=",x.role_id)))
	})
	return pager;
}