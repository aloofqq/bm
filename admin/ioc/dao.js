var ioc={
    dataSource : {
        type : "com.alibaba.druid.pool.DruidDataSource",
        events : {
            depose : 'close'
        },
        fields : {
            url : "jdbc:mysql://localhost:3306/bm?characterEncoding=utf8&useSSL=false&serverTimezone=Hongkong",
            username : "root",
            password : "chaojimima=123$",
            maxActive:50,
            maxWait: 15000, // 若不配置此项,如果数据库未启动,druid会一直等可用连接,卡住启动过程
            filters:"stat",//配置监控统计拦截的filters
            defaultAutoCommit : false // 提高fastInsert的性能
        }
    },
    dao :{
    	type : "jse.dao.Dao",
        args : [{refer:"dataSource"}]
    }
}