package jse.qrcode;

import java.io.File;
import java.util.Map;

import jse.util.JseMap;

public class Qr {

	/**
	 * 
	 * @param param
	 * @return
	 */
	public static JseMap qr(Map<String,Object> param) {
		JseMap tbl=JseMap.from(param);
        QrCreate creator = new QrCreate();  
        QrModel info = new QrModel();  
        if(tbl.containsKey("width")) {
        	info.setWidth(tbl.getInt("width"));
        }else {
        	info.setWidth(400);  
        }
        if(tbl.containsKey("height")) {
        	info.setHeight(tbl.getInt("height"));
        }else {
        	info.setHeight(400);  
        }
        if(tbl.containsKey("fontsize")) {
        	info.setFontSize(tbl.getInt("fontsize"));
        }else {
        	info.setFontSize(24);  
        }
        //"http://bm.81xy.com/project/1/sid/1000"
        info.setContents(tbl.getString("text"));  
        //"D:/tomcat/webapps/ROOT/img/logo.jpg"
        if(tbl.containsKey("img")) {
        	if(tbl.get("img") instanceof File) {
        		info.setLogoFile(tbl.getAs("img",File.class));  
        	}else {
        		info.setLogoFile(new File(tbl.getString("img")));  
        	}
        }
        //"站点编码:1000"
        if(tbl.containsKey("desc")) {
        	info.setDesc(tbl.getString("desc"));  
        }
        //"D:/1.jpg"
        creator.createCodeImage(info,tbl.getString("path"));  
        return tbl;
    }  
	public static void main(String[] args) {
		JseMap map=JseMap.NEW();
		map.put("text", "http://bm.81xy.com/project/1/sid/1000");
		map.put("img", "D:/work/bm/WebContent/img/logo.jpg");
		map.put("desc", "         站点编号1000");
		map.put("path", "d:/1.jpg");
		qr(map);
	}
}
