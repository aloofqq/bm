function chat(tbl){
	let u=user();
	if(u==null){
		return ">>:/login"
	}
	let uid=u.id;
	dao.update("app_chat",Chain.make("status",1),Cnd.where("user_id","=",uid))
	let teacher=dao.fetch("sys_user",Cnd.where("id","=",tbl.id))
	if(teacher!=null){
		let chats=dao.query("app_chat",Cnd.where("tid","=",teacher.id)
				.and("user_id","=",uid))
				chats.forEach(function(x){
					x.put("id","t"+x.tid)
					x.remove("fromid")
					x.put("mine",x.mine==1?true:false)
				})
		attr("chats",chats)
	}else{
		attr("chats",new ArrayList())
	}
	return teacher;
}