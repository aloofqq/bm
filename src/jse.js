var System=Java.type("java.lang.System");
var String=Java.type("java.lang.String");
var Date=Java.type("java.util.Date");
var ArrayList=Java.type("java.util.ArrayList");
var HashMap=Java.type("java.util.HashMap");
var HashSet=Java.type("java.util.HashSet");
var List=java.util.List;
var Arrays=java.util.Arrays;
var Calendar=Java.type("java.util.Calendar");
var File=Java.type("java.io.File");
var Files=Java.type("java.nio.file.Files");
var Charset=Java.type("java.nio.charset.Charset");
var URLEncoder=Java.type("java.net.URLEncoder");
var Cookie=Java.type("javax.servlet.http.Cookie");
var Jse=Java.type("jse.Jse");
var Record=Java.type("jse.dao.Record");
var Cnd=Java.type("jse.dao.Cnd");
var Static=Java.type("jse.dao.util.cri.Static")
var Chain=Java.type("jse.dao.Chain");
var Pager=Java.type("jse.dao.Pager");
var Sqls=Java.type("jse.dao.Sqls");
var Trans=Java.type("jse.trans.Trans")
var Lang=Java.type("jse.util.Lang");
var Strings=Java.type("jse.util.Strings");
var Times=Java.type("jse.util.Times");
var JseMap=Java.type("jse.util.JseMap");
var Streams=Java.type("jse.util.Streams");
var Xmls=Java.type("jse.util.Xmls");
var IP=Java.type("jse.util.IP");
var Captcha=Java.type("jse.util.Captcha");
var Trees=Java.type("jse.util.Trees")
var Tpls=Java.type("jse.util.Tpls")
var Order=Java.type("jse.util.Order")
var MysqlBak=Java.type("jse.util.MysqlBak")
var Fs=Java.type("jse.util.Fs")
var Zips=Java.type("jse.util.Zips")

var Http=Java.type("jse.http.Http");
var Json=Java.type("jse.json.Json");
//quartz
var quartzManager=ioc.get("jse.quartz.QuartzManager");
var JobKey=Java.type("org.quartz.JobKey");
var QuartzJob=Java.type("jse.quartz.QuartzJob");

var print=function(x){System.err.println(x)}
var alert=function(x){System.err.println(x)}
var console=new Object;
console.log=function(x){alert(x)}

var $list=function (table,cnd,page,num){
	if(isEmpty(cnd)){cnd=null;}else if(typeof(cnd)=="string"){cnd=Cnd.wrap(cnd)}
	if(isEmpty(page)){return dao.query(table,cnd)}
	if(typeof(page)=="object"){return dao.query(table,cnd,page)}
	var pager=new Pager(page,num)
	return dao.query(table,cnd,pager)
}
var $pager=function (table,cnd,page,num){
	if(isEmpty(cnd)){
		cnd=null;
	}
	if(typeof(page)=="object")
		return dao.pager(table,cnd,page);
	else if(isEmpty(page))
		return dao.pager(table,cnd);
	else
		return dao.pager(table,cnd,page,num);
	}
var $fetch=function (table,cnd){
	if(isEmpty(cnd)){cnd=null;}else if(typeof(cnd)=="string"){cnd=Cnd.wrap(cnd)}
	return dao.fetch(table,cnd);
}
var $count=function(table,cnd){if(isEmpty(cnd)){cnd=null;}return dao.count(table,cnd)}
var $sql=function(sql,type){return dao.sql(sql,type);}
var $func=function(table,func,col){return dao.func(table, func, col)}
var $save=function (tbl){
	tbl=JseMap.WRAP(tbl)
	if(isEmpty(tbl.id))
		tbl.remove("id")
	if(tbl._httpurl!=null){
		attr("_httpurl",tbl._httpurl)
		tbl.remove("_httpurl")
	}
	try {
		dao.insert(tbl)
	} catch (e) {
		print(e)
		if("Field 'id' doesn't have a default value"==e){
			tbl.put("id",uuid())
			dao.insert(tbl)
		}
	}
	return tbl;
}
var $update=function (tbl,cnd){
	tbl=JseMap.WRAP(tbl)
	if(tbl._httpurl!=null){
		attr("_httpurl",tbl._httpurl)
		tbl.remove("_httpurl")
	}
	if(tbl.containsKey(".table")){
		table=tbl.get(".table");
		tbl.remove(".table")
		if(isEmpty(cnd)){cnd=Cnd.where("id","=",tbl.id);}else if(typeof(cnd)=="string"){cnd=Cnd.wrap(cnd)}
		return dao.update(table,Chain.from(tbl),cnd)
	}
	return -1;
}
var $mager=function (tbl,cnd){if(isEmpty(tbl.id)){tbl.remove("id");$save(tbl);}else{$update(tbl,cnd)}}
function $del(tbl,id){
	var sql=Sqls.create("DELETE FROM $table WHERE id = @id");
	sql.vars().set("table",tbl);
	sql.params().set("id",id);
	return dao.execute(sql);
}
/** dao end! **/
function isEmpty(v){return v==undefined||v==null||v==""||v=="null"}
var reqBody= function(){return Streams.readAndClose(req.getReader())}
var foJson=function(x){if((typeof x)=="object")return x;else return Json.fromJson(x);}
var toJson=function(x){return Json.toJson(x)}
var attr=function(n,v){if(v==undefined){return req.getAttribute(n)}req.setAttribute(n,v)}
var sattr=function(n,v){if(isEmpty(v)){return (session != null ? session.getAttribute(n) : null);}session.setAttribute(n,v)}
var $app=function(n,v){if(v==undefined){return app.getAttribute(n)}app.setAttribute(n,v)}
function user(){
	let u=(session != null ? session.getAttribute("user") : null);
	if(u!=null){
		let u1=dao.fetch("users",Cnd.where("id","=",u.id))
		if(u1!=null){
			return u1;
		}else{
			return null;
		}
	}else{
		return null;
	}
}
var uuid=function(){
	var str = java.util.UUID.randomUUID().toString()
	var temp = str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18) + str.substring(19, 23) + str.substring(24);
	return temp;
}
function addCookie(n,v){
	var cookie = new Cookie(n,v);
	cookie.setMaxAge(31536000000);
	cookie.setPath("/");
	resp.addCookie(cookie);
}
function getCookie(n){
	let v="";
	let cookies = req.getCookies();
    //如果浏览器中存在Cookie
    if (cookies != null && cookies.length > 0) {
        //遍历所有Cookie
        for(let i=0;i<cookies.length;i++) {
        	let cookie=cookies[i]
            //找到name为city的Cookie
            if (cookie.getName().equals(n)) {
                //使用URLDecode.decode()解码,防止中文乱码
            	v=cookie.getValue();
            }
        }
    }
    return v;
}
function delCookie(){
	var cookies = req.getCookies();
	for (var i = 0; i < cookies.length; i++) {
		cookies[i].setMaxAge(0);
		cookies[i].setPath("/");
		resp.addCookie(cookies[i]);
	}
}
var ip=function(){return IP.getIP();}
var isAjax=function(){let type = req.getHeader("X-Requested-With");if(!isEmpty(type)&&type=="XMLHttpRequest"){return true;}else return false;}
function $brow(){
	try {
		print(111)
		print(req.contentType)
		if(req.method=="POST"){
		let tbl=new Record();
		var tab=new Record();
		tab.set(".table","brow");
		var ip=req.getRemoteAddr();
		tab.set("ip",ip)
		var path = req.getRequestURI();
		var url = req.getScheme()+"://"+req.getServerName()+path;
		var param=req.getQueryString();
		tab.set("param",param)
		req.getParameterMap().forEach(function(k, v) {
					tbl.put(k, v.length > 1 ? v : v[0]);
				});
		tab.set("tbl",tbl);
		tab.set("url",url);
		let u=user();
		if(u!=null){
			tab.set("user_id",u.id);
		}
		tab.set("source",req.getHeader("Referer"));
		tab.set("agent",req.getHeader("User-Agent"));
		tab.set("city","");
		$save(tab)
		}
	} catch (e) {
		print(e)
	}
}
//$brow();