function notify_url(tbl){
	let params = new HashMap();
	let requestParams = req.getParameterMap();
	for (let iter = requestParams.keySet().iterator(); iter.hasNext();) {
		let name = iter.next();
		let values =requestParams.get(name);
		let valueStr = "";
		for (let i = 0; i < values.length; i++) {
			valueStr = (i == values.length - 1) ? valueStr + values[i]
					: valueStr + values[i] + ",";
		}
		//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
		//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
		params.put(name, valueStr);
	}

	var conf=$fetch("alipay_config",Cnd.where("id","=",1))
	//商户订单号
	var out_trade_no = tbl.out_trade_no;
	//支付宝交易号
	var trade_no = tbl.trade_no;
	//交易状态
	var trade_status = tbl.trade_status;
	var verify_result=false;
	try {
		let AlipaySignature=Java.type("com.alipay.api.internal.util.AlipaySignature")
		verify_result =AlipaySignature.rsaCheckV1(params, conf.alipay_public_key, "utf-8", "RSA2");
	} catch (e) {
		print("conver alipay order fail!"+e)
	}
	var result=""
	if(verify_result){//验证成功
		if(trade_status=="TRADE_FINISHED"||trade_status=="TRADE_SUCCESS"){
			var order = dao.fetch("app_order", Cnd.where("orderno", "=", out_trade_no));
				if(order==null){
					result= "success";
					return result;
				}
				if(order.status == 1){
					result= "success";
					return result;
				}
				if (order.price != tbl.total_fee) {
					result= "total_fee error!"
				}
				if(trade_status=="TRADE_FINISHED"){
					print("waring!重新付款得！");
				}
				dao.update("app_order",Chain.make("status",1).add("paytype","alipay").add("pay_time",new java.util.Date()),Cnd.where("id","=",order.id));
				try {
					var u=dao.fetch("users",Cnd.where("id","=",order.user_id))
					attr("user",u)
					attr("teacher",dao.fetch("sys_user",Cnd.where("codes","like","%"+u.code+",%").and("role_id","=",2)))
				} catch (e) {
					// TODO: handle exception
				}
		}
		result="success"
		tbl.put("id", uuid());
		tbl.put(".table", "alipay_notify");
		dao.insert(tbl);
		
	}else{//验证失败
		result= "fail";
	} 
	return result;//则不再提示
}