function wxpay(tbl){
	if(user()==null){
		return ">>:/login"
	}
	let order=dao.fetch("app_order",Cnd.where("orderno","=",tbl.orderno))
	let enroll=dao.fetch("app_enroll",Cnd.where("id","=",order.enroll_id))
	let wxpay_notify=dao.fetch("wxpay_notify",Cnd.where("out_trade_no","like",tbl.orderno+"-%"))
	wxpay_notify.put("bank_name",dao.fetch("bank_type",Cnd.where("code","=",wxpay_notify.bank_type)).name)
	attr("enroll",enroll)
	attr("order",order)
	return wxpay_notify;
}