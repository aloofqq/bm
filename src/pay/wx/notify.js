function notify(tbl){
	try{
		tbl=Xmls.xmlToMap(reqBody());
		let result="<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
		
	let orderno=tbl.out_trade_no.substring(0, tbl.out_trade_no.indexOf("-"));
	if(tbl.return_code=="SUCCESS"){
		var order = dao.fetch("app_order", Cnd.where("orderno", "=", orderno));
		if(order.status == 1){
			return result;
		}
		dao.update("app_order",Chain.make("status",1).add("paytype","wxpay").add("pay_time",new java.util.Date()),Cnd.where("id","=",order.id));
		let tab=new Record();
		tab.put(".table", "wxpay_notify");
		tab.put("appid",tbl.appid)
		tab.put("mch_id",tbl.mch_id)
		tab.put("device_info",tbl.device_info)
		tab.put("nonce_str",tbl.nonce_str)
		tab.put("sign",tbl.sign)
		tab.put("sign_type",tbl.sign_type)
		tab.put("result_code",tbl.result_code)
		tab.put("err_code",tbl.err_code)
		tab.put("err_code_des",tbl.err_code_des)
		tab.put("openid",tbl.openid)
		tab.put("is_subscribe",tbl.is_subscribe)
		tab.put("trade_type",tbl.trade_type)
		tab.put("bank_type",tbl.bank_type)
		tab.put("total_fee",tbl.total_fee||-1)
		tab.put("settlement_total_fee",tbl.settlement_total_fee||-1)
		tab.put("fee_type",tbl.fee_type)
		tab.put("cash_fee",tbl.cash_fee||-1)
		tab.put("cash_fee_type",tbl.cash_fee_type)
		tab.put("coupon_fee",tbl.coupon_fee||-1)
		tab.put("coupon_count",tbl.coupon_count||-1)
		tab.put("coupon_type",tbl.coupon_type_0)
		tab.put("coupon_id",tbl.coupon_id_0)
		tab.put("transaction_id",tbl.transaction_id)
		tab.put("out_trade_no",tbl.out_trade_no)
		tab.put("attach",tbl.attach)
		tab.put("time_end",tbl.time_end)
		dao.insert(tab)
		return result;
	}
} catch (e) {
	print(e);
	result=e;
	return "error";
}

}
