function project(tbl){
	let u=user()
	if(u==null){
		tbl.put("goto_url","/project/"+tbl.id+"/sid/"+tbl.sid)
		return ">>:/login?sid="+tbl.sid+"&pid="+tbl.id
	}
	let project=dao.fetch("app_project",Cnd.where("id","=",tbl.id))
	if(project==null||project.status!=1){
		return ">>:/project/not";
	}
	let site=dao.fetch("app_site",Cnd.where("code","=",tbl.sid))
	if(site==null){
		return ">>:/project/notsid";
	}else if(site.status!=1){
		return ">>:/project/not";
	}
	
	let num=dao.count("app_site",Cnd.where("code","=",tbl.sid));
	if(num>0){
			
			//只支持注册站点项目和编码
			/*if(user().pid+""!=tbl.id||user().code!=tbl.sid){
				return ">>:/project/err";
			}*/
		tbl.put("sid",u.code)
			let enroll=dao.fetch("app_enroll",Cnd.where("user_id","=",u.id))
			if(enroll!=null){
				let order=dao.fetch("app_order",Cnd.where("user_id","=",u.id));
				if(order!=null){
					if(order.status==1){//已支付
						//tbl.put("goto_url","/users/")
						return ">>:/users/"
					}else{//未支付
						//tbl.put("goto_url","/users/order")
						return ">>:/users/order"
					}
				}else{
					return ">>:/pay/order/enroll?id="+enroll.id
				}
			}
			
			return tbl;
	}else{
		return ">>:/nologin"
	}
	
}
