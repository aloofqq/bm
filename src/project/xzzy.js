//选择专业
function xzzy(tbl){
	if(user()==null){
		return ">>:/login"
	}
	let zys=new Record();
	let site=dao.fetch("app_site",Cnd.where("code","=",tbl.sid))
	let project=dao.fetch("app_project",Cnd.where("id","=",tbl.id))
	if(!isEmpty(site.zys)){
		zys=site.zys
	}else if(!isEmpty(project.zys)){
		zys=project.zys
	}else{
		zys.put("degree",java.util.List.of("专科","本科"))
		zys.put("zk",dao.select("select name from app_zy where degree='专科'","strs"))
		zys.put("bk",dao.select("select name from app_zy where degree='本科'","strs"))
	}
	if(zys.class.name=="java.lang.String"){
		zys=foJson(zys)
	}
	let bms=new ArrayList();
	zys.degree.forEach(function(d){
		let bm=new Record();
		bm.put("degree",d)
		let list=new ArrayList();
		if(d=="专科"){
		zys.zk.forEach(function(x){
			list.add(dao.fetch("app_zy",Cnd.where("degree","=",d)
					.and("name","=",x)))
		})
		bm.put("zylist",list)
		}else if(d=="本科"){
		zys.bk.forEach(function(x){
			list.add(dao.fetch("app_zy",Cnd.where("degree","=",d)
					.and("name","=",x)))
		})
		bm.put("zylist",list)
		}
		bms.add(bm)
	})
	attr("bms",bms)
	//print(bms)
	return tbl;
}
