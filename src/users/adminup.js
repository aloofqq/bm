function adminup(tbl){
	if(sattr("admin")==null){
		return ">>:/admin/"
	}
	if(typeof(tbl.worktime)=="string"){
		tbl.put("worktime",List.of(tbl.worktime))
	}
	tbl.put("worktime",toJson(tbl.worktime))
	if(tbl.size()>0){
		dao.update("sys_user",Chain.from(tbl),Cnd.where("id","=",sattr("admin").id))
		let admin=dao.fetch("sys_user",Cnd.where("id","=",sattr("admin").id))
		sattr("admin",admin)
	}
	return ">>:/users/admin"
}