function refund_save(tbl){
	if(user()==null){
		return ">>:/login"
	}
	let refund=dao.fetch("app_refund",Cnd.where("orderno","=",tbl.orderno));
	if(refund!=null){
		tbl.put("id",refund.id)
		$update(tbl)
	}else{
		dao.insert(tbl)
	}
	dao.update("app_order",Chain.make("refund",2),Cnd.where("id","=",tbl.oid))
	return ">>:/users/order";
}