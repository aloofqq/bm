function upenroll(tbl){
	if(user()==null){
		return ">>:/login"
	}
	let card=tbl.card.toUpperCase();
	tbl.put("card",card)
	let birthday=card.substring(6,14)
	var region=dao.fetch("sys_region",Cnd.where("code","=",card.substring(0,6)))
	tbl.put("birthday",birthday)
	if(region!=null){
	tbl.put("province",region.province)
	tbl.put("area",region.area)
	tbl.put("county",region.county)
	}
	if(!isEmpty(tbl.yxk)){
		let yxks=tbl.yxk.split(",")
		tbl.put("yxk",yxks[0])
		tbl.put("yxkml",yxks[1])
	}
	if(tbl.get(".table")=="app_enroll_up"){
		let num=dao.count("app_enroll_up",Cnd.where("id","=",tbl.id))
		if(num>0){
			$update(tbl)
		}else
			$save(tbl)
	}else{
		$update(tbl)
	}
	return ">>:/users/order"
}